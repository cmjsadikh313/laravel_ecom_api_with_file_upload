<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function storeImage($request, $pointer, $path = null) //for single file
    {
        if ($request->hasFile($pointer)) {

            $path = $path === null ? '/' . $pointer : $path;
            $path = Storage::disk('public')->put($path, $request->file($pointer));
            return json_encode([
                'name' => basename($path),
                'url' => asset('/storage/' . $path),
                'uri' => $path,
            ]);
        }
    }

     public function storeImages($pointer, $request)//for multiple files
    {
        $files = $request->file($pointer);
        $data = [];
        foreach ($files as $file) {
            $filenameWithExt = $file->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension();
            $NameToStore = $filename . '_' . time() . '.' . $extension;
            $dest = 'public/' . $pointer;
            $path = $file->storeAs($dest, $NameToStore);
            chmod('../public/storage/images/'.$NameToStore, fileperms('../public/storage/images/'.$NameToStore) | 128 + 16 + 2);
            $path = $NameToStore;
            $save = ([
                'name' => basename($path),
                'url' => asset('/storage/' . $pointer . '/' . $path),
                'uri' => $path,
            ]);
            array_push($data, $save);
        }
        return json_encode($data);
    }



}
