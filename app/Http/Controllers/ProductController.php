<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return new ProductCollection(Product::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
         $product = new Product;
        $product->category_id = $request->Input('category_id');
        $product->product_name = $request->Input('product_name');
        $product->price = $request->Input('price');
         if ($request->hasFile('image')) {//single upload
            $product->image = $this->storeImage($request, 'image');
        }

         if ($request->hasFile('images')) {//multiple uploads
            $product->images = $this->storeImages( 'images', $request);
        }


        $product->save();

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        return $request->all();
        $product->category_id = $request->Input('category_id');
        $product->product_name = $request->Input('product_name');
        $product->price = $request->Input('price');
        if ($request->hasFile('image')) {
            $current = json_decode($product->image);
            if ($current != null)
                Storage::delete('public/image/' . $current->name);
            $product->image = $this->storeImage($request, 'image');
        } //else {
        //     if (request('image') == 'deleted') {
        //         $product->image = null;
        //     }
        // }

        ///multiple update

        if ($request->hasFile('images')) {
            $currents = json_decode($product->images);
             if(sizeof($currents)){
            foreach($currents as $current) {
        
                Storage::delete('public/images/' . $current->name);
            $product->images = $this->storeImages('images', $request);
            }
        }
            
        } 


        ///

        $product->save();

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
         $current = json_decode($category->image);
       $product->destroy($product->id);
        if ($current != null)
            Storage::delete('public/image/' . $current->name);

        return new ProductResource($product);
    }
}
